FROM php:8-apache

ENV APACHE_DOCUMENT_ROOT /app/public
ENV ACCEPT_EULA=Y

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite


# https://github.com/mlocati/docker-php-extension-installer
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
  install-php-extensions @composer xdebug apcu opcache pdo_sqlsrv pdo_mysql 

# Install git nodejs npm
RUN apt-get update && apt-get install -y git nodejs npm

WORKDIR /app
